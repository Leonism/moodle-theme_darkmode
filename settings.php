<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings page and admin options.
 *
 * @package   theme_darkmode
 * @copyright 2021 Mohamed Leon Ankar
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// This line protects the file from being accessed by a URL directly.
defined('MOODLE_INTERNAL') || die();

// This is used for performance, we don't need to know about these settings on every page in Moodle.
// only when we are looking at the admin settings pages.
if ($ADMIN->fulltree) {


    // Boost provides a nice setting page which splits settings onto separate tabs. We want to use it here.
    $settings = new theme_boost_admin_settingspage_tabs('themesettingdarkmode', get_string('configtitle', 'theme_darkmode'));

    // Each page is a tab - the first is the "General" tab.
    $page = new admin_settingpage('theme_darkmode_general', get_string('generaltabtitle', 'theme_darkmode'));

    $name = 'theme_darkmode/samecolors';
    $title = get_string('samecolors', 'theme_darkmode');
    $description = get_string('samecolors_desc', 'theme_darkmode');
    $setting = new admin_setting_configcheckbox ($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    $settings->add($page);

    // Light mode page.
    $page = new admin_settingpage('theme_darkmode_light', get_string('lighttabtitle', 'theme_darkmode'));


    // Presets.
    $name = 'theme_darkmode/presetlight';
    $title = get_string('presetlight', 'theme_darkmode');
    $description = get_string('presetlight_desc', 'theme_darkmode');
    $default = 'Boost';

    $choices = [];

    // These are the built in presets.
    $choices['Boost'] = 'Boost';
    $choices['Flatly'] = 'Flatly';
    $choices['Lux'] = 'Lux';

    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Primary Colors.
    $name = 'theme_darkmode/primarycolor_light';
    $title = get_string('primarycolor', 'theme_darkmode');
    $description = get_string('primarycolorlight_desc', 'theme_darkmode');
    $setting = new admin_setting_configcolourpicker($name, $title, $description, "#0f6fc5");
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);


    $name = 'theme_darkmode/secondarycolor_light';
    $title = get_string('secondarycolor', 'theme_darkmode');
    $description = get_string('secondarycolorlight_desc', 'theme_darkmode');
    $setting = new admin_setting_configcolourpicker($name, $title, $description, "#ced4da");
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Must add the page after definiting all the settings.
    $settings->add($page);


    $page = new admin_settingpage('theme_darkmode_dark', get_string('darktabtitle', 'theme_darkmode'));

    // Dark mode presets.
    $name = 'theme_darkmode/presetdark';
    $title = get_string('presetdark', 'theme_darkmode');
    $description = get_string('presetdark_desc', 'theme_darkmode');
    $default = 'Darkly';

    $choices = [];

    $choices['Darkly'] = 'Darkly';
    $choices['Solar'] = 'Solar';
    $choices['Superhero'] = 'Superhero';


    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);


    // Dark mode colors.
    $name = 'theme_darkmode/primarycolor_dark';
    $title = get_string('primarycolor', 'theme_darkmode');
    $description = get_string('primarycolordark_desc', 'theme_darkmode');
    $setting = new admin_setting_configcolourpicker($name, $title, $description, "#00bc8c");
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    $name = 'theme_darkmode/secondarycolor_dark';
    $title = get_string('secondarycolor', 'theme_darkmode');
    $description = get_string('secondarycolordark_desc', 'theme_darkmode');
    $setting = new admin_setting_configcolourpicker($name, $title, $description, "#adb5bd");
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    $settings->add($page);
}
