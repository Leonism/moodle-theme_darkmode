// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * JavaScript AMD modules, that are necessary for theme functionality.
 *
 * @copyright 2021 Mohamed Leon Ankar
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

export const init = () => {

            const toggle = document.getElementById("darktheme-checkbox");

            var darkThemeEnabled = localStorage.getItem("darkThemeEnabled");

            if (darkThemeEnabled === null) {
                darkThemeEnabled = "0";
                localStorage.setItem("darkThemeEnabled", "0");

                toggle.checked = false;
                document.documentElement.setAttribute('data-theme', 'light');


            } else if (darkThemeEnabled == "1") {

                toggle.checked = true;
                document.documentElement.setAttribute('data-theme', 'dark');

            } else {

                toggle.checked = false;
                document.documentElement.setAttribute('data-theme', 'light');
            }

            toggle.addEventListener("change", (e) => {

                if (e.target.checked) {
                    document.documentElement.setAttribute('data-theme', 'dark');
                    localStorage.setItem("darkThemeEnabled", "1");

                } else {
                    document.documentElement.setAttribute('data-theme', 'light');

                    localStorage.setItem("darkThemeEnabled", "0");
                }

            });

            return;
};