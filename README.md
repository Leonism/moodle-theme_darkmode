# moodle-theme_darkmode

This Theme is based on the built in and the default theme Boost that's included in moodle and also includes some extra themes based on Bootswatch.

This plugin gives the user the ability to switch between a bright light normal theme and a dark theme on the fly using a toggler in the navigation bar.

2021 Mohamed Leon Ankar

## Table of contents

* [Features](#Features)
* [Installation](#installation)
* [Screenshots](#Screenshots)
* [Ideas for Future Updates](#ideas-for-future-updates)
* [License](#license)
* [Project status](#project-status)


## Features

* Admins can choose 2 different themes and their primary/secondary colors.
* Users can toggle between 2 themes on the fly by using a toggle in the navigation bar.


## Installation

There are currently 2 ways to install the plugin:
1. [Installing via uploaded ZIP file.](https://docs.moodle.org/311/en/Installing_plugins#Installing_via_uploaded_ZIP_file)
2. [Installing manually at the server.](https://docs.moodle.org/311/en/Installing_plugins#Installing_manually_at_the_server)


## Screenshots

This plugin adds a toggle to the navbar that allows the user to switch between 2 themes on the fly without the need to reload the page.<br><br>
![A toggle between 2 themes](/source/images/toggle.png "Toggle")<br>

An example of how the dashboard can look like.<br><br>
<img src="/source/images/dashboard.png"  width="640" height="360"><br>


## Ideas for Future Updates
* Allow the admin to upload a custom preset.
* Allow the admin to choose the color of the navigation bar and eventually the text in the bar.
* Make the selected mode visible to the user (icon above selecting ball).
* Default color automatically change according to the selected preset.


## License
 This file is part of Moodle - http://moodle.org/

 Moodle is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Moodle is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
 
 @package   theme_darkmode
 
 @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 

## Project status
The project is currently waiting for approval by moodle.
Stable Version 2021111700
